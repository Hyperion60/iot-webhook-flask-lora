from flask import Flask, render_template, request
from datetime import datetime
import json

app = Flask(__name__)

JSON_FILE = {}


@app.route('/add', methods=['POST'])
def add_data():
    JSON_FILE = json.load(open("db.json", "r"))
    JSON_FILE['releves'].append(
        {
            "count": request.json['uplink_message']['decoded_payload']['compteur'],
            "temp": request.json['uplink_message']['decoded_payload']['temp'],
            "hygro": request.json['uplink_message']['decoded_payload']['hygro'],
            "date": request.json['received_at']
        }
    )
    with open("db.json", "w") as file:
        json.dump(JSON_FILE, file)
    return "OK"


@app.route('/')
def hello_world():
    JSON_FILE = json.load(open("db.json", "r"))
    releves = []
    for releve in JSON_FILE['releves']:
        date = datetime.strptime(releve['date'].split('.')[0], "%Y-%m-%dT%H:%M:%S")
        new_releve = [releve['count'], releve['temp'], releve['hygro'], date.date().__str__(), date.time().__str__()]
        releves.append(new_releve)
    return render_template("index.html", releves=releves, title="Releves LoRa")


if __name__ == '__main__':
    app.run(debug=False, port=8064, host='0.0.0.0')
