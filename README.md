# IoT Webhook Flask LoRa

Application pour collecter les données en provenance de The Things Network

## Lancer l'application Flask en local

Veiller a ce que les dépendances soient installées dans l'environnement Python `requirements.txt`

A l'emplacement du fichier `app.py`, ouvrir une fenêtre PowerShell
```powershell
python.exe -m flask run
```

## Lancer le conteneur de l'application

A l'emplacement du fichier `app.py`:

```bash
docker build -t "name:1.0" .
docker run -d -p 8064:8064 name:1.0
```

### Configuration Nginx


*/etc/site-enables/example.com*
```
server {
    server_name example.com;
    client_max_body 10M;
    keepalive_timeout 0;

    location / {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_pass http://127.0.0.1:8064;
    }
}
```
